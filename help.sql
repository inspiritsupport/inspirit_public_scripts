--------------------------------------------------------------------------------
--
-- File name:   help.sql
-- Purpose:     Help
-- Usage:       @help <string>
--
--------------------------------------------------------------------------------
--ACCEPT search_string CHAR PROMPT "Search: [%] "

DEFINE amp=chr(38)
DEFINE nl=chr(10)
DEFINE search_string=:1

COLUMN name FORMAT A25 TRUNC
COLUMN description FORMAT A60 WORD_WRAP
COLUMN usage FORMAT A110

WITH q AS (
SELECT name, description, usage
FROM (
  SELECT 'ac.sql' AS name, 'Display active session' AS description, 'ac'AS usage FROM dual UNION ALL
  SELECT 'ac2.sql' AS name, 'Display active session in detail' AS usage FROM dual UNION ALL
  SELECT 'lsf.sql' AS name, 'Report Log Switch Frequency Map in last N day' AS description, 'lsf <day>' AS usage FROM dual UNION ALL
  SELECT '' AS name, '' AS description, '' AS usage FROM dual UNION ALL
  SELECT '' AS name, '' AS description, '' AS usage FROM dual
  ) 
)
SELECT * FROM q
WHERE
   (upper(name)        LIKE upper ('%&search_string%') OR regexp_like(name, '&search_string', 'i'))
OR (upper(description) LIKE upper ('%&search_string%') OR regexp_like(description, '&search_string', 'i'))
-- OR (upper(usage)       LIKE upper ('%&search_string%') OR regexp_like(usage, '&search_string', 'i'))
ORDER BY
    name
/

UNDEFINE search_string
CLEAR COLUMNS