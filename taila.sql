WITH oneday AS (
SELECT /*+ materialize */ * FROM TABLE(gv$(cursor(select inst_id, originating_timestamp, message_text FROM v$diag_alert_ext 
WHERE ORIGINATING_TIMESTAMP > SYSTIMESTAMP - 1 AND UPPER(filename) LIKE (select '%'||UPPER(name)||'%' from v$database)))))
SELECT INST_ID, TO_CHAR (ORIGINATING_TIMESTAMP, 'YYYY-MM-DD HH24:MI:SS') ORIGINATING_TIMESTAMP, MESSAGE_TEXT FROM oneday
 WHERE originating_timestamp > sysdate-600/1440
ORDER BY 2 DESC;