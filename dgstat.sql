SELECT a.dest_id,
       a.name,
       a.thread#,
       c.status,
       c.type,
       c.database_mode,
       c.recovery_mode,
       c.protection_mode,
       d.process,
       d.transmit_mode,
       b.last_seq LAST_SEQ_PRMY,
       a.applied_seq APPLIED_SEQ_STBY,
       TO_CHAR (a.last_app_timestamp, 'YYYY-MM-DD HH24:MI:SS') LAST_APP_TIMESTAMP,
       b.last_seq - a.applied_seq ARCH_DIFF,
       FLOOR(SYSDATE - last_app_timestamp) || 'd ' ||
       TRUNC( 24*((SYSDATE-last_app_timestamp) -
       TRUNC(SYSDATE-last_app_timestamp))) || 'h ' ||
       MOD(TRUNC(1440*((SYSDATE-last_app_timestamp) -
       TRUNC(SYSDATE-last_app_timestamp))), 60) ||'m ' ||
       MOD(TRUNC(86400*((SYSDATE-last_app_timestamp) -
       TRUNC(SYSDATE-last_app_timestamp))), 60) ||'s' TIME_DIFF,
       c.error
  FROM ( SELECT thread#,
                 MAX (sequence#) applied_seq,
                 MAX (next_time) last_app_timestamp,
                 dest_id,
                 name
            FROM gv$archived_log
           WHERE applied = 'YES'
             AND name in (select destination from gv$archive_dest where destination is not null)
             AND first_change# > (SELECT resetlogs_change# FROM v$database)
             AND resetlogs_time = (SELECT resetlogs_time FROM v$database)
        GROUP BY thread#, dest_id, name) a,
       ( SELECT thread#, MAX (sequence#) last_seq, dest_id
            FROM gv$archived_log
           WHERE resetlogs_time = (SELECT resetlogs_time FROM v$database)
           AND first_change# > (SELECT resetlogs_change# FROM v$database)
        GROUP BY thread#,dest_id) b,
        ( SELECT
       dest_id,
       dest_name,
       status,
       TYPE,
       database_mode,
       recovery_mode,
       protection_mode,
       destination,
       db_unique_name,
       error
  FROM v$archive_dest_status
 WHERE TYPE <> 'LOCAL'
 ) c,
 (select dest_id,archiver,process,transmit_mode FROM v$archive_dest where destination is not null) d
 WHERE a.thread# = b.thread#
   AND a.dest_id = c.dest_id
   AND b.dest_id = c.dest_id
   AND a.dest_id = d.dest_id
   AND b.dest_id = d.dest_id
   AND c.dest_id = d.dest_id
 ORDER BY 2, 3