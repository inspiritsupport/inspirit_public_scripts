select
TO_CHAR("Reserved_Space(MB)", '999,999,999,999') as "Reserved_Space(MB)",
TO_CHAR("Reserved_Space(MB)" - "Free_Space(MB)", '999,999,999,999') as "Reserved_Space(MB)",
TO_CHAR("Free_Space(MB)", '999,999,999,999') as "Free_Space(MB)"
from(
select 
(select (sum(bytes)/1024/1024) from v$datafile ) "Reserved_Space(MB)",
(select (sum(bytes)/1024/1024) from dba_free_space) "Free_Space(MB)"
from dual);