SELECT
  m.tablespace_name,
  df.df_count df_cnt,
  df.non_ext_datafile non_ext_df_cnt,
  to_char(m.tablespace_size * t.block_size/1024/1024, '999,999,999,999') AS max_mb,
  to_char(m.used_space * t.block_size/1024/1024, '999,999,999,999') AS used_mb,
  to_char((m.tablespace_size-m.used_space) * t.block_size/1024/1024, '999,999,999,999') free_mb,
  round(m.used_percent,1) used_pct
FROM
  dba_tablespace_usage_metrics m
  JOIN dba_tablespaces t ON m.tablespace_name = t.tablespace_name,
  (SELECT tablespace_name,COUNT(*) df_count,(COUNT(*) - SUM(decode(autoextensible, 'YES', 1, 0))) non_ext_datafile
   FROM dba_data_files WHERE status = 'AVAILABLE' GROUP BY tablespace_name) df
WHERE df.tablespace_name=m.tablespace_name
ORDER BY 7 DESC;

Prompt SELECT * FROM dba_data_files WHERE tablespace_name='TSNAME' ORDER BY 2 DESC;

Prompt ALTER TABLESPACE TSNAME ADD DATAFILE 'E:\ORACLE\ORADATA\SKY\TSNAME01.DBF' SIZE 1M AUTOEXTEND ON NEXT 512M MAXSIZE UNLIMITED;