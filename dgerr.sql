  SELECT inst_id,
         severity,
         ERROR_CODE,
         TO_CHAR (timestamp, 'DD-MON-YYYY HH24:MI:SS')     "timestamp",
         MESSAGE
    FROM gv$dataguard_status
   WHERE timestamp > SYSDATE - :1 / 1440
ORDER BY timestamp DESC;