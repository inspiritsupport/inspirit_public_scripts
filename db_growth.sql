SET SERVEROUT ON

DECLARE
    v_ts_id                      NUMBER;
    not_in_awr                   EXCEPTION;
    v_ts_block_size              NUMBER;
    v_begin_snap_id              NUMBER;
    v_end_snap_id                NUMBER;
    v_begin_snap_date            DATE;
    v_end_snap_date              DATE;
    v_numdays                    NUMBER;
    v_count                      NUMBER;
    v_ts_begin_size              NUMBER;
    v_ts_end_size                NUMBER;
    v_ts_growth                  NUMBER;
    v_ts_begin_allocated_space   NUMBER;
    v_ts_end_allocated_space     NUMBER;
    v_db_begin_size              NUMBER := 0;
    v_db_end_size                NUMBER := 0;
    v_db_begin_allocated_space   NUMBER := 0;
    v_db_end_allocated_space     NUMBER := 0;
    v_db_growth                  NUMBER := 0;

    CURSOR v_cur IS
        SELECT tablespace_name
          FROM dba_tablespaces
         WHERE contents = 'PERMANENT';
BEGIN
    FOR v_rec IN v_cur
    LOOP
        BEGIN
            v_ts_begin_allocated_space := 0;
            v_ts_end_allocated_space := 0;
            v_ts_begin_size := 0;
            v_ts_end_size := 0;

            SELECT ts#
              INTO v_ts_id
              FROM v$tablespace
             WHERE name = v_rec.tablespace_name;

            SELECT block_size
              INTO v_ts_block_size
              FROM dba_tablespaces
             WHERE tablespace_name = v_rec.tablespace_name;

            SELECT COUNT (*)
              INTO v_count
              FROM dba_hist_tbspc_space_usage
             WHERE tablespace_id = v_ts_id;

            IF v_count = 0
            THEN
                RAISE not_in_awr;
            END IF;

            SELECT MIN (snap_id),
                   MAX (snap_id),
                   MIN (TRUNC (TO_DATE (rtime, 'MM/DD/YYYY HH24:MI:SS'))),
                   MAX (TRUNC (TO_DATE (rtime, 'MM/DD/YYYY HH24:MI:SS')))
              INTO v_begin_snap_id,
                   v_end_snap_id,
                   v_begin_snap_date,
                   v_end_snap_date
              FROM dba_hist_tbspc_space_usage
             WHERE tablespace_id = v_ts_id;

            IF UPPER (v_rec.tablespace_name) = 'SYSTEM'
            THEN
                v_numdays := v_end_snap_date - v_begin_snap_date;
            END IF;

            SELECT ROUND (
                       MAX (tablespace_size) * v_ts_block_size / 1024 / 1024,
                       2)
              INTO v_ts_begin_allocated_space
              FROM dba_hist_tbspc_space_usage
             WHERE tablespace_id = v_ts_id AND snap_id = v_begin_snap_id;

            SELECT ROUND (
                       MAX (tablespace_size) * v_ts_block_size / 1024 / 1024,
                       2)
              INTO v_ts_end_allocated_space
              FROM dba_hist_tbspc_space_usage
             WHERE tablespace_id = v_ts_id AND snap_id = v_end_snap_id;

            SELECT ROUND (
                         MAX (tablespace_usedsize)
                       * v_ts_block_size
                       / 1024
                       / 1024,
                       2)
              INTO v_ts_begin_size
              FROM dba_hist_tbspc_space_usage
             WHERE tablespace_id = v_ts_id AND snap_id = v_begin_snap_id;

            SELECT ROUND (
                         MAX (tablespace_usedsize)
                       * v_ts_block_size
                       / 1024
                       / 1024,
                       2)
              INTO v_ts_end_size
              FROM dba_hist_tbspc_space_usage
             WHERE tablespace_id = v_ts_id AND snap_id = v_end_snap_id;

            v_db_begin_allocated_space :=
                v_db_begin_allocated_space + v_ts_begin_allocated_space;
            v_db_end_allocated_space :=
                v_db_end_allocated_space + v_ts_end_allocated_space;
            v_db_begin_size := v_db_begin_size + v_ts_begin_size;
            v_db_end_size := v_db_end_size + v_ts_end_size;
            v_db_growth := v_db_end_size - v_db_begin_size;
        END;
    END LOOP;

    DBMS_OUTPUT.put_line (CHR (10));
    DBMS_OUTPUT.put_line ('Summary');
    DBMS_OUTPUT.put_line ('========');
    DBMS_OUTPUT.put_line (
           '1) Allocated Space: '
        || TRIM(TO_CHAR (v_db_end_allocated_space, '999,999,999,999'))
        || ' MB');

    DBMS_OUTPUT.put_line (
           '2) Used Space: '
        || TRIM(TO_CHAR (v_db_end_size, '999,999,999,999'))
        || ' MB');

    DBMS_OUTPUT.put_line (
           '3) Used Space Percentage: '
        || TRIM(TO_CHAR (v_db_end_size / v_db_end_allocated_space * 100, '999,999,999,999'))
        || '%');

    DBMS_OUTPUT.put_line (CHR (10));
    DBMS_OUTPUT.put_line ('History');
    DBMS_OUTPUT.put_line ('========');
    DBMS_OUTPUT.put_line (
           '1) Allocated Space on '
        || to_char(v_begin_snap_date, 'DD-MON-YY')
        || ': '
        || TRIM(TO_CHAR (v_db_begin_allocated_space, '999,999,999,999'))
        || ' MB');

    DBMS_OUTPUT.put_line (
           '2) Current Allocated Space on '
        || to_char(v_end_snap_date, 'DD-MON-YY')
        || ': '
        || TRIM(TO_CHAR (v_db_end_allocated_space, '999,999,999,999'))
        || ' MB');

    DBMS_OUTPUT.put_line (
           '3) Used Space on '
        || to_char(v_begin_snap_date, 'DD-MON-YY')
        || ': '
        || TRIM(TO_CHAR (v_db_begin_size, '999,999,999,999'))
        || ' MB');

    DBMS_OUTPUT.put_line (
           '4) Current Used Space on '
        || to_char(v_end_snap_date, 'DD-MON-YY')
        || ': '
        || TRIM(TO_CHAR (v_db_end_size, '999,999,999,999'))
        || ' MB');

    DBMS_OUTPUT.put_line (
           '5) Total growth during last '
        || v_numdays
        || ' days between '
        || to_char(v_begin_snap_date, 'DD-MON-YY')
        || ' and '
        || to_char(v_end_snap_date, 'DD-MON-YY')
        || ': '
        || TRIM(TO_CHAR (v_db_growth, '999,999,999,999'))
        || ' MB');

    IF (v_db_growth <= 0 OR v_numdays <= 0)
    THEN
        DBMS_OUTPUT.put_line (CHR (10));
        DBMS_OUTPUT.put_line ('No data growth was found for the Database');
    ELSE
        DBMS_OUTPUT.put_line (
               '6) Per day growth during last '
            || v_numdays
            || ' days: '
            || TRIM(TO_CHAR (v_db_growth / v_numdays, '999,999,999,999'))
            || ' MB');

        DBMS_OUTPUT.put_line (CHR (10));
        DBMS_OUTPUT.put_line ('Expected Growth');
        DBMS_OUTPUT.put_line ('===============');
        DBMS_OUTPUT.put_line (
               '1) Expected growth for next 30 days: '
            || TRIM(TO_CHAR ((v_db_growth / v_numdays) * 30, '999,999,999,999'))
            || ' MB');

        DBMS_OUTPUT.put_line (
               '2) Expected growth for next 60 days: '
            || TRIM(TO_CHAR ((v_db_growth / v_numdays) * 60, '999,999,999,999'))
            || ' MB');

        DBMS_OUTPUT.put_line (
               '3) Expected growth for next 90 days: '
            || TRIM(TO_CHAR ((v_db_growth / v_numdays) * 90, '999,999,999,999'))
            || ' MB');

    END IF;
EXCEPTION
    WHEN not_in_awr
    THEN
        DBMS_OUTPUT.put_line (CHR (10));
        DBMS_OUTPUT.put_line (
            '====================================================================================================================');
        DBMS_OUTPUT.put_line (
            '!!! ONE OR MORE TABLESPACES USAGE INFORMATION NOT FOUND IN AWR !!!');
        DBMS_OUTPUT.put_line (
            'Execute DBMS_WORKLOAD_REPOSITORY.CREATE_SNAPSHOT,or wait for next AWR snapshot capture before executing this script');
        DBMS_OUTPUT.put_line (
            '====================================================================================================================');
END;
/