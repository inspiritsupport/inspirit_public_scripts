SELECT
    username,
    error_number,
    error_text,
    instance_number inst_id,
    sql_text,
    session_user,
    current_user,
    current_schema,
    host,
    ip_address,
    os_user,
    terminal,
    service_name,
    to_char(error_date, 'DD/MON/YY HH24:MI:SS') error_date
FROM
    inspirit.ins_error_summary
WHERE
    error_date > sysdate -  :1 / 1440
  --error_date BETWEEN TO_DATE ('30.09.2019 08:30:00','dd.mm.yyyy hh24:mi:ss') AND TO_DATE ('30.09.2019 09:30:00','dd.mm.yyyy hh24:mi:ss')
ORDER BY
    error_date DESC;