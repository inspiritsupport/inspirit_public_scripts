SELECT 
  inst_id||':'||sid||','||serial# AS "User Session",
  username,
  osuser,
  machine,
  program,
  module,
  sql_id,
  sql_exec_id,
  sql_hash_value,
  event,
  wait_class,
  floor(last_call_et/86400) || 'd ' || to_char(to_date(mod  (last_call_et,86400) ,'sssss'),'hh24"h" mi"m" ss"s"') last_call_et
FROM gv$session
WHERE type NOT LIKE 'BACKGROUND'
AND STATUS LIKE 'ACTIVE'
ORDER BY username,osuser;