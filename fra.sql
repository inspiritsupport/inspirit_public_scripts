Prompt Flash Recovery Area Usage Detail
Prompt ========================
SELECT * FROM V$FLASH_RECOVERY_AREA_USAGE;

Prompt Flash Recovery Area Status
Prompt ========================
SELECT
  NAME,
  to_char((A.SPACE_LIMIT / 1024 / 1024), '999,999,999,999') total_mb,
  to_char((A.SPACE_USED / 1024 / 1024), '999,999,999,999') used_mb,
  ROUND(SUM(B.PERCENT_SPACE_USED),1) used_pct
FROM
  V$RECOVERY_FILE_DEST A,
  V$FLASH_RECOVERY_AREA_USAGE B
GROUP BY
  NAME,
  SPACE_LIMIT,
  SPACE_USED;

Prompt After that you can resize the FRA with:
Prompt ========================
Prompt ALTER SYSTEM SET db_recovery_file_dest_size=xxG;
Prompt
Prompt Or change the FRA to a new location (new archives will be created to this new location):
Prompt ========================
Prompt ALTER SYSTEM SET DB_RECOVERY_FILE_DEST='/u....';
Prompt