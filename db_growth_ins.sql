select to_char(t1.record_date,'YYYY-MM') tarih, t1.total_gb,
round( (total_gb - (lead(total_gb,1) over (order by record_date desc))) / (lead(total_gb,1) over (order by record_date desc)) * 100, 2) yuzde_buyume
from (
select record_date, cnt, total_bytes, total_gb from
(select dense_rank() over (partition by trunc(record_date,'MM') order by record_date asc) drank, t.* from
(select record_date, count(*) cnt, sum(bytes) total_bytes, round(sum(bytes)/1024/1024/1024,2) total_gb from INSPIRIT.INS_SEG_ENLARGEMENT
group by record_date
order by record_date desc) t)
where drank=1
order by record_date desc) t1
where rownum < 100;