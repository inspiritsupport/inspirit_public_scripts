prompt ASM Disks In Use
prompt ================

select group_number  "Group"
,      disk_number   "Disk"
,      header_status "Header"
,      mode_status   "Mode"
,      state
,      TO_CHAR(create_date, 'DD-Mon-YYYY HH24:MI:SS') created
,      redundancy
,      TO_CHAR(total_mb, '999,999,999,999') total_mb
,      TO_CHAR(free_mb, '999,999,999,999')  free_mb
,      name          "Disk Name"
--,      failgroup     "Failure Group"
,      path
--,      read_time     "ReadTime"
--,      write_time    "WriteTime"
--,      bytes_read/1073741824    "BytesRead"
--,      bytes_written/1073741824 "BytesWrite"
--,      read_time/reads "SecsPerRead"
--,      write_time/writes "SecsPerWrite"
from v$asm_disk_stat
where header_status not in ('FORMER','CANDIDATE')
order by group_number, disk_number;
 
Prompt File Types in Diskgroups
Prompt ========================

select g.name                                   "Group Name"
,      f.TYPE                                   "File Type"
,      f.BLOCK_SIZE/1024||'k'                   "Block Size"
,      f.STRIPED
,        count(*)                               "Files"
,      round(sum(f.BYTES)/(1024*1024*1024),2)   "Gb"
from   v$asm_file f,v$asm_diskgroup g
where  f.group_number=g.group_number
group by g.name,f.TYPE,f.BLOCK_SIZE,f.STRIPED
order by 1,2;

Prompt ASM Disks Status
Prompt ========================

SELECT g.group_number gn
,      g.name         name
,      g.state        state
,      g.type         type
,      to_char((g.total_mb/decode(g.type,'NORMAL',2,'HIGH',3,'EXTERN',1)), '999,999,999,999') total_mb
,      to_char((g.free_mb/decode(g.type,'NORMAL',2,'HIGH',3,'EXTERN',1)), '999,999,999,999') free_mb
,      to_char((g.total_mb/decode(g.type,'NORMAL',2,'HIGH',3,'EXTERN',1))- (g.free_mb/decode(g.type,'NORMAL',2,'HIGH',3,'EXTERN',1)), '999,999,999,999') used_mb
,      ROUND(((g.total_mb/decode(g.type,'NORMAL',2,'HIGH',3,'EXTERN',1))-(g.free_mb/decode(g.type,'NORMAL',2,'HIGH',3,'EXTERN',1)))/(g.total_mb/decode(g.type,'NORMAL',2,'HIGH',3,'EXTERN',1))*100,1) used_pct
FROM v$asm_disk d, v$asm_diskgroup g
WHERE d.group_number = g.group_number and
d.group_number <> 0 and
d.state = 'NORMAL' and
d.mount_status = 'CACHED'
GROUP BY g.group_number, g.name, g.state, g.type, g.total_mb, g.free_mb
ORDER BY used_pct DESC;
