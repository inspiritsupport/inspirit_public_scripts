with x as (
select to_char(end_time,'DD-MON-YYYY') as "DATE",to_char (end_time, 'Dy') day, instance_number, metric_name, count(*) cnt, avg(average) avg_iops, max(maxval) max_iops
from dba_hist_sysmetric_summary
where metric_name in ('Physical Read Total IO Requests Per Sec','Physical Write Total IO Requests Per Sec')
and end_time > sysdate - :1
group by to_char(end_time,'DD-MON-YYYY'), to_char (end_time, 'Dy'), instance_number, metric_name
order by 1 desc, 2, 3
),
y as (
select "DATE",day,instance_number, metric_name, sum(avg_iops) avg_iops, sum(max_iops) max_iops
from x
group by "DATE",day,instance_number, metric_name
)
select "DATE",day ,instance_number inst_id, metric_name, trunc(avg(avg_iops),2) avg_iops, trunc(max(max_iops),2) max_iops
from y
group by "DATE",day,instance_number, metric_name
order by 1 desc, 3;